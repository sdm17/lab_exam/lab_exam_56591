const express = require('express')
const cors = require('cors')

const bookRouter = require('./routes/book')

const app = express()

app.use(cors('*'))

app.use(express.json())

app.use('/book',bookRouter)

app.listen(4000,"0.0.0.0",()=>{
    console.log('server started on port 4000')
    console.log('Test change in jenkins auto build')
})


