const e = require('express')
const { response } = require('express')
const express = require('express')
const { request } = require('http')
const db = require('../db')
const utils =  require('../utils')
const router = express.Router()

router.get('/:bookName',(request,response)=>{
    const {bookName} = request.params

    const statement = `select * from book where book_title='${bookName}'`

    db.execute(statement,(error,result)=>{
        if(error){
            response.send(utils.createResults(error))
        }else{
            response.send(utils.createResults(error,result))
        }
    })

})

router.post('/add-book',(request,response)=>{
    // book_id, book_title, publisher_name, author_name
    const {bookTitle,publisherName,authorName} = request.body

    const statement = `insert into book (book_title,publisher_name,author_name)
    values ('${bookTitle}','${publisherName}','${authorName}')`

    db.execute(statement,(error,result)=>{
        if(error){
            response.send(utils.createResults(error))
        }else{
            response.send(utils.createResults(error,result))
        }
    })

})

router.put('/update/:id',(request,response)=>{
    const {id} = request.params
    // publisher_name and Author_name
    const {publisherName,authorName} = request.body

    const statement = `update book 
    SET publisher_name= '${publisherName}',
        author_name='${authorName}' 
    where book_id='${id}'`

    db.execute(statement,(error,result)=>{
        if(error){
            response.send(utils.createResults(error))
        }else{
            response.send(utils.createResults(error,result))
        }
    })

})

router.delete('/delete/:id',(request,response)=>{
    const {id} = request.params
    console.log('Jenkins Change Reflection Check')
    const statement = `delete from book where book_id='${id}'`

    db.execute(statement,(error,result)=>{
        if(error){
            response.send(utils.createResults(error))
        }else{
            response.send(utils.createResults(error,result))
        }
    })

})


module.exports = router