-- Book(book_id, book_title, publisher_name, author_name)
CREATE TABLE book(book_id int primary key auto_increment,
                    book_title varchar(25),
                    publisher_name varchar(25),
                    author_name varchar(25));